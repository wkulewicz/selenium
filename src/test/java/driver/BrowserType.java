package driver;

public enum BrowserType {

    FIREFOX("firefox"),
    CHROME("chrome"),
    CHROME_DEBUG("chrome_debug"),
    IE("internetexplorer");

    private final String browser;

    BrowserType(String browser) {
        this.browser = browser;
    }

}