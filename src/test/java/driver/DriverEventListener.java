package driver;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;
import java.util.Random;

public class DriverEventListener implements WebDriverEventListener {


    public void beforeAlertAccept(WebDriver driver) {

    }


    public void afterAlertAccept(WebDriver driver) {

    }


    public void afterAlertDismiss(WebDriver driver) {

    }


    public void beforeAlertDismiss(WebDriver driver) {

    }


    public void beforeNavigateTo(String url, WebDriver driver) {

    }


    public void afterNavigateTo(String url, WebDriver driver) {

    }


    public void beforeNavigateBack(WebDriver driver) {

    }


    public void afterNavigateBack(WebDriver driver) {

    }


    public void beforeNavigateForward(WebDriver driver) {

    }


    public void afterNavigateForward(WebDriver driver) {

    }


    public void beforeNavigateRefresh(WebDriver driver) {

    }


    public void afterNavigateRefresh(WebDriver driver) {

    }


    public void beforeFindBy(By by, WebElement element, WebDriver driver) {

    }


    public void afterFindBy(By by, WebElement element, WebDriver driver) {

    }


    public void beforeClickOn(WebElement element, WebDriver driver) {
        try {
            gentleWait();
            checkForRobotModal(driver);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void afterClickOn(WebElement element, WebDriver driver) {
        try {
            gentleWait();
            checkForRobotModal(driver);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        gentleWait();
    }


    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        gentleWait();
    }


    public void beforeScript(String script, WebDriver driver) {

    }


    public void afterScript(String script, WebDriver driver) {

    }


    public void beforeSwitchToWindow(String windowName, WebDriver driver) {

    }


    public void afterSwitchToWindow(String windowName, WebDriver driver) {

    }


    public void onException(Throwable throwable, WebDriver driver) {

    }

    public <X> void beforeGetScreenshotAs(OutputType<X> target) {

    }


    public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {

    }

    public void beforeGetText(WebElement webElement, WebDriver webDriver) {

    }


    public void afterGetText(WebElement webElement, WebDriver webDriver, String s) {

    }

    public static void gentleWait() {
        Random random = new Random();
        int rand = random.nextInt(50) + 20;

        try {
            Thread.sleep(rand);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public static void checkForRobotModal(WebDriver driver) throws InterruptedException {
        if (driver.findElements(By.xpath("//*[contains(text(),'robot')]")).size() > 0) {
            for (int i = 0; i < 1000; i++) {
                java.awt.Toolkit.getDefaultToolkit().beep();
                Thread.sleep(1000);
            }
        }
    }
}