package driver.manager;

import driver.BrowserType;
import driver.BrowserFactory;
import driver.DriverEventListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class DriverManager {

    private static final BrowserType BROWSER_TYPE = BrowserType.CHROME_DEBUG;
    private static WebDriver driver;

    private DriverManager() {
    }

    public static WebDriver getWebDriver() {


        if (driver == null) {
            driver = BrowserFactory.getBrowser(BROWSER_TYPE);
        }

        DriverEventListener driverEventListener = new DriverEventListener();
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        driver = eventFiringWebDriver.register(driverEventListener);

        return driver;
    }

    public static void disposeDriver() {
        driver.close();
        if (!BROWSER_TYPE.equals(BrowserType.FIREFOX)){
            driver.quit();
        }
        driver = null;
    }

    public BrowserType getBrowserType(){
        return BROWSER_TYPE;
    }
}