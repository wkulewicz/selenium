package tests;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.objects.*;
import java.io.IOException;

public class BuyPlayersTests extends TestBase {

    @Parameters({"cardType", "leagueName","maxBid","profit"})
    @Test
    public void buyPlayers(String cardType, String leagueName, String maxBid, int profit) throws IOException, ParseException, InterruptedException {

        MainMenuPage mainMenuPage = new MainMenuPage();
        mainMenuPage
                .clickOnTransfersButton()
                .clickOnTransferMarketButton()
                .chooseCardType(cardType)
                .chooseLeague(leagueName)
                .insertMaxBid(maxBid)
                .clickSearchButton()
                .betOnPlayers(cardType,leagueName,profit);
    }
}