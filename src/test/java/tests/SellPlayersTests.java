package tests;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.objects.*;
import java.io.IOException;

public class SellPlayersTests extends TestBase {

    @Parameters({"cardType", "leagueName","offerDuration","marginAmount"})
    @Test
    public void putWonPlayersOnAuction(String cardType, String leagueName, String offerDuration, int marginAmount) throws IOException, ParseException, InterruptedException {
        MainMenuPage mainMenuPage = new MainMenuPage();
        mainMenuPage
                .clickOnTransfersButton()
                .clickOnTransferTargetsButton()
                .putPlayersOnAuction(cardType,leagueName,offerDuration,marginAmount);
    }

    @Parameters({"cardType", "leagueName","offerDuration","marginAmount"})
    @Test
    public void putPlayersOnAuctionAgain(String cardType, String leagueName, String offerDuration, int marginAmount) throws IOException, ParseException, InterruptedException {
        MainMenuPage mainMenuPage = new MainMenuPage();
        mainMenuPage
                .clickOnTransfersButton()
                .clickOnTransferListButton()
                .putPlayersOnAuction(cardType,leagueName,offerDuration,marginAmount);
    }
}