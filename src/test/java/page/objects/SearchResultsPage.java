package page.objects;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driver.manager.DriverManager;
import page.objects.utils.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import waits.WaitForElement;

import java.io.IOException;
import java.util.List;


public class SearchResultsPage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//button[@class='flat pagination next']")
    WebElement nextPageButton;

    @FindBy(xpath = "//button[@class='flat pagination prev']")
    WebElement prevPageButton;

    @FindBy(xpath = "//li[contains(@class,'listFUTItem has-auction-data')][last()]")
    WebElement lastPlayer;

    @FindBy(xpath = "//li[@class='listFUTItem has-auction-data']")
    List<WebElement> playerOffers;

    @FindBy(xpath = "//div[@class='auction-state']//span[@class='time'][contains(text(),'s')]")
    WebElement thirtySecondsLeft;

    @FindBy(xpath = "//div[@class='auction-state']//span[@class='time'][contains(text(),'<')]")
    List<WebElement> underMinuteLeft;

    @FindBy(xpath = ".//span[@class='time' and text()='Zakończone']")
    List<WebElement> finishedAuction;

    @FindBy(xpath = "//button[text()='Zalicytuj']")
    WebElement bidButton;

    private WebDriver driver;
    private Boolean someAuctionHasSecondLeft;

    public SearchResultsPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }


    public void betOnPlayers(String cardType, String leagueName, int profit) throws IOException, ParseException, InterruptedException {
        for (int pageNumber = 1; pageNumber < 10; pageNumber++) {
            Thread.sleep(2000); //because of async
            PageFactory.initElements(DriverManager.getWebDriver(), this);
            java.util.Iterator<WebElement> i = playerOffers.iterator();

            someAuctionHasSecondLeft = false;

            if(underMinuteLeft.size() == 0) {
                logger.info("Too long to wait");
                WaitForElement.waitUntilElementIsClickable(lastPlayer);
                lastPlayer.click();
                logger.info("Trying to click Prev Page Button");
                WaitForElement.waitUntilElementIsClickable(prevPageButton);
                prevPageButton.click();
                continue;
            }

            while (i.hasNext()) {
                WebElement playerOffer = i.next();

                if(playerOffer.findElements(By.xpath(".//span[@class='time' and text()='Zakończone' or text()='Przetwarzanie…']")).size() != 0) continue;


                WaitForElement.waitUntilElementIsVisible(playerOffer.findElement(By.xpath(".//div[@class='name' and string-length( text()) > 0]")));
                WaitForElement.waitUntilElementIsVisible(playerOffer.findElement(By.xpath(".//div[@class='rating' and string-length( text()) > 0]")));

                String currentPlayerLastName = playerOffer.findElement(By.xpath(".//div[@class='name']")).getText();
                String currentPlayerRating = playerOffer.findElement(By.xpath(".//div[@class='rating']")).getText();

                int currentPlayerOffer;
                try {
                    currentPlayerOffer = Integer.parseInt(playerOffer.findElement(By.xpath(".//div[@class='auctionValue'][1]/span[@class='currency-coins value']")).getAttribute("innerHTML"));
                } catch (Exception e) {
                    currentPlayerOffer = Integer.parseInt(playerOffer.findElement(By.xpath(".//div[@class='auctionStartPrice auctionValue'][1]/span[@class='currency-coins value']")).getAttribute("innerHTML"));
                }

                Player currentPlayer = new Player(cardType, leagueName, currentPlayerLastName, Integer.parseInt(currentPlayerRating));
                Player foundPlayer = currentPlayer.findInJson();

                if (foundPlayer != null) {
                    int marketPrice = foundPlayer.getPrice();

                    if (currentPlayerOffer + profit <= marketPrice) {

                        if(!someAuctionHasSecondLeft) {
                            logger.info(   "Wait for seconds left...");
                            WaitForElement.waitUntilElementIsVisible(thirtySecondsLeft);
                            someAuctionHasSecondLeft = true;
                        }

                        logger.info(   "Trying to click " + foundPlayer.getLastName());
                        playerOffer.click();
                        logger.info(   "Clicked on " + foundPlayer.getLastName());
                        logger.info( foundPlayer.getLastName() + " market Price is: "+ marketPrice + " I offer "+ currentPlayerOffer);
                        bidButton.click();
                        logger.info(   "Clicked on Bid Button");
                        continue;
                    }
                };
            }
            WaitForElement.waitUntilElementIsClickable(lastPlayer);
            lastPlayer.click();
            WaitForElement.waitUntilElementIsVisible(nextPageButton);
            logger.info("Trying to click Next Page Button");
            nextPageButton.click();
        }
    }
}