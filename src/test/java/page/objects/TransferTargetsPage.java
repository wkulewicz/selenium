package page.objects;

import driver.manager.DriverManager;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.objects.utils.Player;
import waits.WaitForElement;

import java.io.IOException;
import java.util.List;

public class TransferTargetsPage {

    @FindBy(xpath = "//span[text()='Wystaw na licytację']")
    WebElement putOnAuctionSpan;

    @FindBy(xpath = "(//input[@class='numericInput filled'])[1]")
    WebElement startingPriceInput;

    @FindBy(xpath = "(//input[@class='numericInput filled'])[2]")
    WebElement buyNowPriceInput;

    @FindBy(xpath = "//button[text()='Wystaw na licytację']")
    WebElement putOnAuctionButton;

    @FindBy(xpath = "//div[@class='inline-list-select ut-drop-down-control']")
    WebElement auctionTimeDropdown;

    public TransferTargetsPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public void putPlayersOnAuction(String cardType, String leagueName, String offerDuration, int marginAmount) throws IOException, ParseException {
        List<WebElement> boughtPlayers = DriverManager.getWebDriver().findElements(By.xpath("//li[contains(@class,'won')]"));
        java.util.Iterator<WebElement> i = boughtPlayers.iterator();


        while (i.hasNext()) {
            WebElement boughtPlayer = i.next();

            WaitForElement.waitUntilElementIsVisible(boughtPlayer.findElement(By.xpath(".//div[@class='name' and string-length( text()) > 0]")));
            WaitForElement.waitUntilElementIsVisible(boughtPlayer.findElement(By.xpath(".//div[@class='rating' and string-length( text()) > 0]")));

            String currentPlayerLastName = boughtPlayer.findElement(By.xpath(".//div[@class='name']")).getText();
            String currentPlayerRating = boughtPlayer.findElement(By.xpath(".//div[@class='rating']")).getText();
            Player currentPlayer = new Player(cardType, leagueName, currentPlayerLastName, Integer.parseInt(currentPlayerRating));
            Player foundPlayer = currentPlayer.findInJson();

            if (foundPlayer != null) {
                int marketPrice = foundPlayer.getPrice();
                int myStartingPrice = marketPrice + marginAmount - 100;
                int myBuyNowPrice = marketPrice + marginAmount;

                boughtPlayer.click();
                putOnAuctionSpan.click();
                startingPriceInput.click();
                startingPriceInput.sendKeys(String.valueOf(myStartingPrice));
                buyNowPriceInput.click();
                buyNowPriceInput.sendKeys(String.valueOf(myBuyNowPrice));
                auctionTimeDropdown.click();
                DriverManager.getWebDriver().findElement(By.xpath("//li[text()='" + offerDuration + " godz.']")).click();
                putOnAuctionButton.click();

            }
        }
    }
}
