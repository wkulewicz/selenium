package page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driver.manager.DriverManager;
import waits.WaitForElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TransfersPage {

    private WebDriver driver;
    private Logger logger = LogManager.getRootLogger();

    @FindBy(className = "ut-tile-transfer-market")
    private WebElement transferMarketButton;

    @FindBy(className = "ut-tile-transfer-list")
    private WebElement transferListButton;

    @FindBy(className = "ut-tile-transfer-targets")
    private WebElement transferTargetsButton;

    public TransfersPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public TransferMarketPage clickOnTransferMarketButton(){
        logger.info("Trying to click Transfer Market Button");
        WaitForElement.waitUntilElementIsClickable(transferMarketButton);
        transferMarketButton.click();
        logger.info("Clicked on Transfer Market Button");
        return new TransferMarketPage();
    }

    public TransferListPage clickOnTransferListButton(){
        logger.info("Trying to click Transfer List Button");
        WaitForElement.waitUntilElementIsClickable(transferMarketButton);
        transferListButton.click();
        logger.info("Clicked on Transfer List Button");
        return new TransferListPage();
    }

    public TransferTargetsPage clickOnTransferTargetsButton(){
        logger.info("Trying to click Transfer Targets Button");
        WaitForElement.waitUntilElementIsClickable(transferTargetsButton);
        transferTargetsButton.click();
        logger.info("Clicked on Transfer Targets Button");
        return new TransferTargetsPage();
    }

}