package page.objects.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class Player {

    private Logger logger = LogManager.getRootLogger();

    private String cardType;
    private String leagueName;
    private String lastName;
    private int price;
    private int rating;


    public Player(String cardType, String leagueName, String lastName, int rating, int price) {
        this.cardType = cardType;
        this.leagueName = leagueName;
        this.lastName = lastName;
        this.rating = rating;
        this.price = price;
    }

    public Player(String cardType, String leagueName, String lastName, int rating) {
        this.cardType = cardType;
        this.leagueName = leagueName;
        this.lastName = lastName;
        this.rating = rating;
    }

    public String getCardType() {
        return this.cardType;
    }

    public String getLeagueName() {
        return this.leagueName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getPrice() {
        return this.price;
    }

    public int getRating() {
        return this.rating;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Player findInJson() throws IOException, ParseException {

        String cardType = this.getCardType();
        String leagueName = this.getLeagueName();

        JSONParser parser = new JSONParser();
        JSONArray playersDataJson = (JSONArray) parser
                .parse(new FileReader("C:\\workDirectory\\FutData\\" + cardType + "\\" + leagueName + ".json"));

        for (int i = 0; i < playersDataJson.size(); i++) {
            JSONObject player = (JSONObject) playersDataJson.get(i);

                Player foundPlayer = new Player(
                        cardType,
                        leagueName,
                        player.get("lastName").toString(),
                        Integer.parseInt(player.get("rate").toString()),
                        Integer.parseInt(player.get("price").toString())
                );

            if (this.lastName.equals(foundPlayer.getLastName()) && this.rating == foundPlayer.getRating()) {
                logger.info("Returned player: " + foundPlayer.getLastName() + " " + foundPlayer.getRating() + " " + foundPlayer.getPrice());
                return foundPlayer;
            }
        };
        return null;
    }
}
