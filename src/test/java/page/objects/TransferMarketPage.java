package page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driver.manager.DriverManager;
import waits.WaitForElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TransferMarketPage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//img[contains(@src,'level')]/..")
    WebElement cardTypeDropDown;

    @FindBy(xpath = "//li[text()='Srebrny']")
    WebElement silverCardTypeButton;

    @FindBy(xpath = "//li[text()='Złoty']")
    WebElement goldCardTypeButton;

    @FindBy(xpath = "//li[text()='Specjalny']")
    WebElement specialCardTypeButton;

    @FindBy(xpath = "//img[contains(@src,'league')]/..")
    WebElement leagueDropDown;

    @FindBy(xpath = "//li[text()='Premier League (ENG 1)']")
    WebElement engLeagueButton;

    @FindBy(xpath = "//li[text()='LaLiga Santander (ESP 1)']")
    WebElement espLeagueButton;

    @FindBy(xpath = "//li[text()='Serie A TIM (ITA 1)']")
    WebElement itaLeagueButton;

    @FindBy(xpath = "//li[text()='Bundesliga (GER 1)']")
    WebElement gerLeagueButton;

    @FindBy(xpath = "//li[text()='Ligue 1 Conforama (FRA 1)']")
    WebElement fraLeagueButton;

    @FindBy(xpath = "(//input)[3]")
    WebElement maxBidInput;

    @FindBy(xpath = "//button[text()='Szukaj']")
    WebElement searchButton;

    private WebDriver driver;

    public TransferMarketPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public TransferMarketPage chooseCardType(String cardType) {
        logger.info("Trying to click Card Type Dropdown");
        WaitForElement.waitUntilElementIsClickable(cardTypeDropDown);
        cardTypeDropDown.click();
        logger.info("Clicked on Card Type Dropdown");

        logger.info("Trying to click " + cardType + " Card Type Button");
        switch (cardType) {
            case "silver":
                silverCardTypeButton.click();
                break;
            case "gold":
                goldCardTypeButton.click();
                break;
            case "special":
                specialCardTypeButton.click();
                break;
        }
        logger.info("Clicked on  " + cardType + " Card Type Button");
        return this;
    }

    public TransferMarketPage chooseLeague(String league) {
        logger.info("Trying to click League Dropdown");
        WaitForElement.waitUntilElementIsClickable(cardTypeDropDown);
        leagueDropDown.click();
        logger.info("Clicked on CLeague Dropdown");

        logger.info("Trying to click " + league + " League");
        switch (league) {
            case "premierLeague":
                engLeagueButton.click();
                break;
            case "laLiga":
                espLeagueButton.click();
                break;
            case "bundesLiga":
                gerLeagueButton.click();
                break;
            case "league1":
                fraLeagueButton.click();
                break;
            case "serieA":
                itaLeagueButton.click();
                break;
        }
        logger.info("Clicked on  " + league + " League");
        return this;
    }

    public TransferMarketPage insertMaxBid(String maxBid) {
        logger.info("Trying to set " + maxBid + " as maximum bid");
        WaitForElement.waitUntilElementIsClickable(maxBidInput);
        //maxBidInput.click();
        maxBidInput.clear();
        maxBidInput.sendKeys(maxBid);
        logger.info("Maximum bid set");
        return this;
    }

    public SearchResultsPage clickSearchButton() {
        logger.info("Trying to click Search Button");
        WaitForElement.waitUntilElementIsClickable(searchButton);
        searchButton.click();
        logger.info("Clicked on Search Button");
        return new SearchResultsPage();
    }
}