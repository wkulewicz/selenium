package page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driver.manager.DriverManager;
import waits.WaitForElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class MainMenuPage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(css = ".icon-transfer")
    private WebElement transfersButton;

    private WebDriver driver;

    public MainMenuPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public TransfersPage clickOnTransfersButton(){
        logger.info("Trying to click Transfers Button");
        WaitForElement.waitUntilElementIsClickable(transfersButton);
        transfersButton.click();
        logger.info("Clicked on Transfers Button");
        return new TransfersPage();
    }

}